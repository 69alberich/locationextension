<?php namespace Qchsoft\LocationExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftLocationextensionShippingPrices extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_locationextension_shipping_prices', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->decimal('shipping_price', 10, 2);
            $table->decimal('min_shipping_price', 10, 2);
            $table->integer('shippable_id');
            $table->string('shippable_type', 150);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_locationextension_shipping_prices');
    }
}
