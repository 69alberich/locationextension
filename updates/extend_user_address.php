<?php namespace Qchsoft\LocationExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class ExtendUserAddress extends Migration
{    
    const TABLE_NAME = 'lovata_orders_shopaholic_user_addresses';
    public function up()
    {
        if (!Schema::hasTable(self::TABLE_NAME) || Schema::hasColumn(self::TABLE_NAME, 'model_id')
        || Schema::hasColumn(self::TABLE_NAME, 'model_type')) {
            return;
        }
        Schema::table(self::TABLE_NAME, function (Blueprint $obTable){
            $obTable->integer('model_id')->nullable(0);
            $obTable->string('model_type')->nullable(0);
        });
    }

    public function down()
    {
       if (!Schema::hasTable(self::TABLE_NAME) || !Schema::hasColumn(self::TABLE_NAME, 'model_id')
       || !Schema::hasColumn(self::TABLE_NAME, 'model_type')) {
            return;
        }

        Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            
            $obTable->dropColumn('model_id');
            $obTable->dropColumn('model_type');
        });
    }
}
