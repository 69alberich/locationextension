<?php namespace Qchsoft\LocationExtension\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class creating_zone_id_in_order extends Migration
{
    const TABLE_NAME = 'lovata_orders_shopaholic_orders';
    
    public function up()
    {
        // Schema::create('qchsoft_yatchextension_table', function($table)
        // {
        // });
        if (!Schema::hasTable(self::TABLE_NAME) || Schema::hasColumn(self::TABLE_NAME, 'zone_id')) {
            return;
        }
        
         Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            $obTable->integer('zone_id')->default(0);
        });
    }

    public function down()
    {
        if (!Schema::hasTable(self::TABLE_NAME) || !Schema::hasColumn(self::TABLE_NAME, 'zone_id')) {
            return;
        }

        Schema::table(self::TABLE_NAME, function (Blueprint $obTable)
        {
            
            $obTable->dropColumn('zone_id');
    
        });
    }
}