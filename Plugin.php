<?php namespace Qchsoft\LocationExtension;
use Qchsoft\LocationExtension\Classes\Event\CityModelHandler;
use Qchsoft\LocationExtension\Classes\Event\ZoneModelHandler;
use Qchsoft\LocationExtension\Classes\Event\CityControllerHandler;
use Qchsoft\LocationExtension\Classes\Event\OrderControllerHandler;
use Qchsoft\LocationExtension\Classes\Event\OrderModelHandler;
use Qchsoft\LocationExtension\Classes\Event\UserAddressModelHandler;

use Lovata\Ordersshopaholic\Models\UserAddress;

use QchSoft\Location\Models\City;
use System\Classes\PluginBase;
use Event;
class Plugin extends PluginBase
{
    public function registerComponents(){
        return [
            'QchSoft\LocationExtension\Components\AddressHandler' => 'AddressHandler',
        ];
    }

    public function registerSettings(){

    }

    protected function addEventListener(){
        Event::subscribe(CityControllerHandler::class);
        Event::subscribe(CityModelHandler::class);
        Event::subscribe(ZoneModelHandler::class);
        Event::subscribe(OrderControllerHandler::class);
        Event::subscribe(OrderModelHandler::class);
        Event::subscribe(UserAddressModelHandler::class);


        Event::listen(\Lovata\OrdersShopaholic\Classes\Processor\OrderProcessor::EVENT_GET_SHIPPING_PRICE, function($arOrderData) {
            //$arOrderData - array with order data from request
            //Calculate shipping price
        
            if ($arOrderData["shipping_type_id"] != 1 ) {
            $cityId = $arOrderData["property"]["shipping_city"];

                $city = City::find($cityId);
                
                return $city->getPrice();
            }else{
                return 0;
            }
            
        });

        
        Event::listen('shopaholic.order.created', function($obOrder) {
           

            $user = $obOrder->user;

            $obAddressList = UserAddress::getByUser($user->id)->get();
            if ($obAddressList->isEmpty() && $obOrder->shipping_type->code="delivery") {
               
              $arAddress = [
                  "address1" => $obOrder->getProperty("shipping_address1"),
                  "type" => "shipping",
                  "model_id" => $obOrder->zone_id,
                  "model_type" => "Qchsoft\Location\Models\Zone",
                  "user_id" => $user->id
              ];

              $address = UserAddress::create($arAddress);

            }
        });
        
    }

    public function boot(){
        $this->addEventListener();
    }
}
