<?php namespace Qchsoft\LocationExtension\Models;

use Model;

/**
 * Model
 */
class ShippingPrices extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_locationextension_shipping_prices';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $morphTo = [
        'shippable' => []
    ];

    public $fillable = [
        'shipping_price',
        'min_shipping_price',
    ];

    public static function getFromModel($model){
        if($model->ship_price){
            return $model->ship_price;
        }else{
            $shipPrice = new static();
            $shipPrice->shippable = $model;
            $shipPrice->shipping_price = 0;
            $shipPrice->min_shipping_price = 0;
            $shipPrice->save();

            $model->ship_price = $shipPrice;

            return $shipPrice;
        }
    }

}
