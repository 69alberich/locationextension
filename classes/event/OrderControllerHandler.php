<?php namespace Qchsoft\LocationExtension\Classes\Event;



use Lovata\OrdersShopaholic\Models\Order as OrderModel;
use Lovata\OrdersShopaholic\Controllers\Orders as OrderController;

/**
 * Class ExtendProductFieldsHandler
 * @package Lovata\BaseCode\Classes\Event\Product
 */
class OrderControllerHandler {

    public function subscribe(){

        OrderController::extendFormFields(function ($form, $model, $context) {
            // Prevent extending of related form instead of the intended User form
            if (!$model instanceof OrderModel) {
               
                return;
            }
            
            $form->addTabFields([
                'zone' => [
                    'label'   => 'Zone',
                    'type'    => 'relation',
                    'nameFrom' => 'name',
                    'span' => 'left',
                    'tab' => 'lovata.ordersshopaholic::lang.tab.shipping_address',
                ],
            ]);
            
            
        });

       
    }
}
