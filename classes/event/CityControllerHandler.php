<?php namespace Qchsoft\LocationExtension\Classes\Event;

use Qchsoft\Location\Controllers\City as CityController;
use Qchsoft\Location\Models\City as CityModel;
use QchSoft\LocationExtension\Models\ShippingPrices as ShippingPricesModel;
class CityControllerHandler {

    public function subscribe(){

        CityController::extend(function($controller) {

            if (!isset($controller->relationConfig)) {
                $controller->addDynamicProperty('relationConfig');
            }
        
            
            $myConfigPath = '$/qchsoft/locationextension/config/shipping_price_relation.yaml';
            $controller->relationConfig = $myConfigPath;
           
            $importConfigPath = '$/qchsoft/locationextension/config/custom_config_city_import.yaml';
        
            $controller->importExportConfig = $controller->mergeConfig(
                $controller->importExportConfig,
                $importConfigPath
            );

        });

        CityController::extendFormFields(function ($form, $model, $context) {
            // Prevent extending of related form instead of the intended User form
    
            if (!$model instanceof CityModel) {
               
                return;
            }
            
            if(!$model->exists){
                return;
            }

            ShippingPricesModel::getFromModel($model);
            
            $form->addTabFields([
                
                'ship_price[shipping_price]' =>[
                    'label' => 'Shipping Price',
                    'type' => 'number',
                    'span' => 'auto',
                    'tab' => 'Shipping Price',
                    'context' => ["update", "preview"]

                ],
                'ship_price[min_shipping_price]' =>[
                    'label' => 'Min total price for shipping',
                    'type' => 'number',
                    'span' => 'auto',
                    'tab' => 'Shipping Price',
                    'context' => ["update", "preview"]

                ]
                
            ]);
            
        });

     }
}
