<?php namespace Qchsoft\LocationExtension\Classes\Event;

use Qchsoft\Location\Models\Zone as ZoneModel;
use Event;

class ZoneModelHandler{

    public function subscribe(){

        ZoneModel::extend(function($model) {
            
             $model->morphMany = [
                'address' => [\Lovata\Ordersshopaholic\Models\UserAddress::class, 'name' => 'location']
            ];

        });
        

    }

}
