<?php namespace Qchsoft\LocationExtension\Classes\Event;

use Lovata\Ordersshopaholic\Models\UserAddress as UserAddressModel;

class UserAddressModelHandler{

    public function subscribe(){
        UserAddressModel::extend(function($model) {
            $model->addFillable(["model_id"]);
            $model->addFillable(["model_type"]);

            

            $model->addCachedField(['model_id']);
            $model->addCachedField(['model_type']);
            

            $model->addDynamicMethod('getLocation', function() use ($model) {
                $class = $model->model_type;
                $location = null;
                if($class != ""){
                    $location = $class::find($model->model_id);
                }
                
                return $location;

            });
        });
    }

}