<?php namespace Qchsoft\LocationExtension\Classes\Event;

use Qchsoft\Location\Models\City as CityModel;
use QchSoft\LocationExtension\Models\ShippingPrices;
use Lovata\Shopaholic\Classes\Helper\CurrencyHelper;
use Lovata\Toolbox\Classes\Helper\PriceHelper;
use Qchsoft\Location\Classes\Import\CitiesImport;
use Event;

class CityModelHandler{

    public function subscribe(){

        CityModel::extend(function($model) {

            $model->morphOne['ship_price'] = ['Qchsoft\LocationExtension\Models\ShippingPrices',
             'name' => 'shippable'];

            
             $model->morphMany = [
                'location' => [Lovata\Ordersshopaholic\Models\UserAddress::class, 'name' => 'location',
                'key'=> "model_id", 'other_key' => "model_type"]
            ];

             $model->addDynamicMethod('isAvailableForShipping', function($mount) use ($model) {

                $defaultCurrency = CurrencyHelper::instance()->getDefault();
                $activeCurrencyCode = CurrencyHelper::instance()->getActiveCurrencyCode();
                if($activeCurrencyCode == "USD"){
                    $convertedPrice = $mount;
                }else{
                    $convertedPrice = $mount/$defaultCurrency->rate;
                }
                if($model->ship_price->min_shipping_price > 0 && $convertedPrice >= $model->ship_price->min_shipping_price){
                    return true;
                }elseif($model->ship_price->min_shipping_price == 0){
                    return true;
                }else{
                    return false;
                }
            });

            $model->addDynamicMethod('getPrice', function() use ($model){

               
                $defaultCurrency = CurrencyHelper::instance()->getDefault();
                $activeCurrencyCode = CurrencyHelper::instance()->getActiveCurrencyCode();

                if($activeCurrencyCode == "USD"){
                    $convertedPrice = $model->ship_price->shipping_price;
                }else{
                    $convertedPrice = $model->ship_price->shipping_price*$defaultCurrency->rate; 
                }

                return $convertedPrice;
                

            });

            $model->addDynamicMethod('getMinPrice', function() use ($model){

               
                $defaultCurrency = CurrencyHelper::instance()->getDefault();
                $activeCurrencyCode = CurrencyHelper::instance()->getActiveCurrencyCode();

                if($activeCurrencyCode == "USD"){
                    $convertedPrice = $model->ship_price->min_shipping_price;
                }else{
                    $convertedPrice = $model->ship_price->min_shipping_price*$defaultCurrency->rate; 
                }
                return PriceHelper::format($convertedPrice) ;
                
            });

        });
        
        

        /*Event::listen(CitiesImport::EVENT_BEFORE_IMPORT, function($row, $items){
            $row["code"] = "modificado";
            trace_log("aqui tengo:");
            trace_log($row);
            return $row;
        });*/

        Event::listen(CitiesImport::EVENT_AFTER_IMPORT, function($obModel, $row){
            if (!$obModel instanceof CityModel) {
                return;
            }
            if (isset($row["shipping_price"]) || isset($row["min_shipping_price"])) {

                $obShippingPrice = ShippingPrices::where("shippable_id", $obModel->id)->first();

                if ($obShippingPrice == null) {
                    
                    $obShippingPrice = new ShippingPrices();
                    $obShippingPrice->shippable_id = $obModel->id;
                    $obShippingPrice->shippable_type = $obModel::class;
                    $obShippingPrice->fill($row);
                    $obShippingPrice->save();


                }else{
                    $obShippingPrice->fill($row);
                    $obShippingPrice->save();
                }
            }   
            
        });

    }

}
/*
public function getConvertedMount($mount){
    $defaultCurrency = CurrencyHelper::instance()->getDefault();
    $activeCurrencyCode = CurrencyHelper::instance()->getActiveCurrencyCode();
   //echo ($defaultCurrency);
   if($activeCurrencyCode == "USD"){
    $convertedPrice = $mount;
   }else{
    $convertedPrice = $mount/$defaultCurrency->rate;
   }
   return $convertedPrice;
}*/