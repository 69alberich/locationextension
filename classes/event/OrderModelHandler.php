<?php namespace Qchsoft\LocationExtension\Classes\Event;

use Lovata\OrdersShopaholic\Models\Order as OrderModel;

class OrderModelHandler{

    public function subscribe(){

        OrderModel::extend(function($model) {
            
            $model->fillable[] = 'zone_id';
            $model->addCachedField(['zone_id']);

            $model->belongsTo['zone'] = ['\Qchsoft\Location\Models\Zone',
             'key' => 'zone_id'];
        });

        
        
    }

}
