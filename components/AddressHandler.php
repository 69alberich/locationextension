<?php namespace QchSoft\LocationExtension\Components;

use QchSoft\Location\Models\Country;
use QchSoft\Location\Models\State;
use QchSoft\Location\Models\City;
use QchSoft\Location\Models\Zone;
use Lovata\Ordersshopaholic\Models\UserAddress;

use Input;
use Response;

class AddressHandler extends \Cms\Classes\ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Address Handler',
            'description' => 'Manage dropdowns of countries, states and cities.'
        ];
    }

    
    public function states($countryId=null){
        $states = State::select(["id", "name", "active"]);
        if ($countryId) {
            $states->where("country_id", $countryId);
        }
        return $states->get();;
    }

    public function cities($stateId=null){
        $cities = City::select(["id", "name", "active"]);
        if ($stateId) {
            $cities->where("state_id", $stateId);
        }
        return $cities->get();
    }

    public function zones($cityId=null){
        $cities = Zone::select(["id", "name", "active"]);
        if ($cityId) {
            $cities->where("city_id", $cityId);
        }
        return $cities->get();
    }

    public function onChangeCountry(){
        $data = post();
        $this->page['states'] = $this->states($data["country_id"]);
        //trace_log($data);
    }

    public function onChangeState(){
        $data = post();
        $this->page['cities'] = $this->cities($data["state_id"]);
        //trace_log($data);
    }

    public function onChangeCity(){
        $data = post();
        $this->page['zones'] = $this->zones($data["city_id"]);
        //trace_log($data);
    }

    public function onSelectSavedAddress(){
        $data = post();
        $address = UserAddress::where("id", $data["address_id"])->first();
        $arAddress = $address->toArray();
        $location = $address->getLocation();

        
        if (get_class($location) == "Qchsoft\Location\Models\Zone") {
          $arAddress["city_id"] = $location->city_id;
        }
       
        
        if ($address != null) {
            return Response::json(['success' => true, "content" => $arAddress]);
        }else{
            return Response::json(['success' => false, "content" => null]);
        }
        

    }

    public function onGetAddress(){
        $data = post();
        $address = UserAddress::where("id", $data["address_id"])->first(); 

        $this->page["address"] = $address;
        
    }

}
